﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Components/HealthComponent.h"

#include "PickUps/PickUpData/HealthPickUpData.h"

DEFINE_LOG_CATEGORY(LogHealthComponent)

UHealthComponent::UHealthComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
	bWantsInitializeComponent = true;
	
	CurrentHealth = MaxHealth;
}

void UHealthComponent::InitializeComponent()
{
	Super::InitializeComponent();

	const auto Owner = GetOwner();
	if (!Owner)
	{
		UE_LOG(LogHealthComponent, Error, TEXT("No owner specified!"));
		return;
	}

	Owner->OnTakeAnyDamage.AddDynamic(this, &UHealthComponent::TakeDamage);
	SetHealth(MaxHealth);
}

void UHealthComponent::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	const auto Owner = GetOwner();
	if (!Owner)
	{
		UE_LOG(LogHealthComponent, Error, TEXT("No owner specified!"));
	}
	else
	{
		HealthChangedEvent.Clear();
		Owner->OnTakeAnyDamage.RemoveDynamic(this, &UHealthComponent::TakeDamage);
		DeathEvent.Clear();
	}
	Super::EndPlay(EndPlayReason);
}

void UHealthComponent::LogDataInternal()
{
	if (GetOwner())
	{
		UE_LOG(LogHealthComponent, Display, TEXT("Max Health = %f"), MaxHealth);
		UE_LOG(LogHealthComponent, Display, TEXT("Current Health = %f"), CurrentHealth);
	}
	
}

void UHealthComponent::LogStateInternal()
{
	if (GetOwner())
	{
		UE_LOG(LogHealthComponent, Display, TEXT("%s damage applied to owner"),
			GetOwner()->OnTakeAnyDamage.Contains(this, "TakeDamage") ? *FString("Process") : *FString("Don't process"));
		UE_LOG(LogHealthComponent, Display, TEXT("Health Event %s"),
			HealthChangedEvent.IsBound() ? TEXT("is bound") : TEXT("is not bound"));
		UE_LOG(LogHealthComponent, Display, TEXT("Death Event %s"),
			DeathEvent.IsBound() ? TEXT("is bound") : TEXT("is not bound"));
	}
}

void UHealthComponent::LogStatement(ELogType Type, ELogStatementType StatementType)
{
	UE_LOG(LogHealthComponent, Warning, TEXT("%s.HealthComponent %s log %s -------|"),
		*GetOwner()->GetName(), Type==ELogType::Data ? *FString("data") : *FString("state"),
		StatementType==ELogStatementType::Begin ? *FString("begins") : *FString("ends"))
}

void UHealthComponent::GetPickableDataClasses(TArray<FPickerDataStruct>& PickableClasses)
{
	PickableClasses.Add(FPickerDataStruct(UHealthPickUpData::StaticClass(), this));
}

bool UHealthComponent::TryPickUp(UPickUpData* Data)
{
	 if (!CanPickUp()) return false;
	
	const auto HealthData = Cast<UHealthPickUpData>(Data);
	if (!HealthData) return false;

	float& DataPoints = HealthData->GetHealthPoints();
	const float NewHealth = CurrentHealth+DataPoints;
	SetHealth(NewHealth);

	if (HealthData->IsSingleUse() || NewHealth-MaxHealth<=0) HealthData->ClearData();
	else
	{
		DataPoints = NewHealth - MaxHealth;
	}
	return true;
}

void UHealthComponent::SetHealth(float NewHealth)
{
	const auto ClampedHealth = FMath::Clamp(NewHealth, 0.f, MaxHealth);

	CurrentHealth = ClampedHealth;
	HealthChangedEvent.Broadcast(CurrentHealth);
}

void UHealthComponent::TakeDamage(AActor* DamagedActor, float Damage, const UDamageType* DamageType,
	AController* InstigatedBy, AActor* DamageCauser)
{
	if (GetOwner() && DamagedActor==GetOwner())
	{
		SetHealth(CurrentHealth-Damage);

		Log(ELogType::Data);
		Log(ELogType::State);
		if (CurrentHealth == 0.f)
		{
			DeathEvent.Broadcast(InstigatedBy, DamageCauser);
		}
	}
}

bool UHealthComponent::CanPickUp()
{
	return CurrentHealth < MaxHealth;
}

