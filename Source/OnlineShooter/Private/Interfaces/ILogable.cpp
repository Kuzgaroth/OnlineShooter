﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "..\..\Public\Interfaces\ILogable.h"

void IILogable::Log(ELogType Type)
{
	LogStatement(Type, ELogStatementType::Begin);
	if (Type==ELogType::Data) LogDataInternal();
	else LogStateInternal();
	LogStatement(Type, ELogStatementType::End);
}
