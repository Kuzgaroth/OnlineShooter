﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "PickUps/HealthPickUp.h"
#include "PickUps/PickUpData/HealthPickUpData.h"


AHealthPickUp::AHealthPickUp()
{
	PrimaryActorTick.bCanEverTick = false;
}

void AHealthPickUp::SetupData()
{
	Super::SetupData();

	auto HealthData = Cast<UHealthPickUpData>(PickUpData);
	if (HealthData)
	{
		HealthData->SetMaxHealthPoints(HealthStructData.MaxHealthPoint);
	}
}



