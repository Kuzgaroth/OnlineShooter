﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "PickUps/PickUpBase.h"
#include "Components/SphereComponent.h"
#include "Interfaces/IPicker.h"
#include "PickUps/PickUpData/PickUpData.h"

DEFINE_LOG_CATEGORY(LogPickUpBase)

APickUpBase::APickUpBase()
{
	PrimaryActorTick.bCanEverTick = false;

	TriggerComponent = CreateDefaultSubobject<USphereComponent>("TriggerSphere");
	TriggerComponent->SetupAttachment(RootComponent);
}


void APickUpBase::BeginPlay()
{
	Super::BeginPlay();
	
}

void APickUpBase::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	SetupData();
	PickUpData->RefreshData();
	if (bPickable) TriggerComponent->OnComponentBeginOverlap.AddDynamic(this, &APickUpBase::OnTriggerOverlapped);
}

void APickUpBase::LogDataInternal()
{
	TArray<FString> DataArray;
	PickUpData->GetDataLog(DataArray);
	for (auto DataLine : DataArray)
	{
		UE_LOG(LogPickUpBase, Display, TEXT("%s"), *DataLine)
	}
}

void APickUpBase::LogStateInternal()
{
	UE_LOG(LogPickUpBase, Display, TEXT("Object is %s and has %s"),
		bPickable ? *FString("pickable") : *FString("not pickable"),
		bPickedUp ? *FString("picked up") : *FString("not picked up"))
}

void APickUpBase::LogStatement(ELogType Type, ELogStatementType StatementType)
{
	UE_LOG(LogPickUpBase, Warning, TEXT("%s %s log %s -------|"), *GetHumanReadableName(),
		Type==ELogType::Data ? *FString("data") : *FString("state"),
		StatementType==ELogStatementType::Begin ? *FString("begins") : *FString("ends"))
}

UPickUpData* APickUpBase::GetPickUpData() const
{
	return PickUpData;
}

void APickUpBase::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	TriggerComponent->OnComponentBeginOverlap.RemoveDynamic(this, &APickUpBase::OnTriggerOverlapped);
	Super::EndPlay(EndPlayReason);
}

void APickUpBase::SetupData()
{
	if (PickUpDataClass)
		PickUpData =  NewObject<UPickUpData>(GetTransientPackage(), PickUpDataClass);
	else PickUpData = NewObject<UPickUpData>();

	PickUpData->SetSingleUse(BaseStructData.bSingleUse);
}


bool APickUpBase::CanBePickedUp()
{
	return bPickable && !bPickedUp;
}

void APickUpBase::OnPickedUp()
{
	bPickedUp=true;
	Destroy();
}

void APickUpBase::OnTriggerOverlapped(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (!OtherActor) return;
	
	const auto Picker = Cast<IIPicker>(OtherActor);
	if (!Picker) return;

	if (CanBePickedUp())
	{
		if (Picker->TryPickUp(PickUpData) && !PickUpData->IsSingleUse())
		{
			Log(ELogType::Data);
		}
		if (PickUpData->IsCleared()) OnPickedUp();
	}
	Log(ELogType::State);
}
