﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "PickUps/PickUpData/HealthPickUpData.h"

UHealthPickUpData::UHealthPickUpData()
{
	UHealthPickUpData::RefreshData();
}

void UHealthPickUpData::SetMaxHealthPoints(float MaxPoints)
{
	MaxHealthPoints = MaxPoints;
}

void UHealthPickUpData::GetDataLog(TArray<FString>& DataArray)
{
	Super::GetDataLog(DataArray);

	DataArray.Add(FString::Printf(TEXT("PickUp max health points = %f"), MaxHealthPoints));
	DataArray.Add(FString::Printf(TEXT("PickUp remained health points = %f"), HealthPoints));
}

void UHealthPickUpData::RefreshData()
{
	Super::RefreshData();

	HealthPoints = MaxHealthPoints;
}

void UHealthPickUpData::ClearData()
{
	Super::ClearData();

	HealthPoints=0.f;
}
