﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "PickUps/PickUpData/PickUpData.h"

void UPickUpData::GetDataLog(TArray<FString>& DataArray)
{
	DataArray.Add(FString::Printf(TEXT("bSingleUse = %s"),
		bSingleUse ? *FString("true") : *FString("false")));
}
