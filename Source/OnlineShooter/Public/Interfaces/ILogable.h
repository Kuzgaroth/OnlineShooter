﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "ILogable.generated.h"


UINTERFACE(MinimalAPI)
class UILogable : public UInterface
{
	GENERATED_BODY()
};

enum class ELogType : uint8
{
	Data,
	State,
	Max
};

enum class ELogStatementType : uint8
{
	Begin,
	End,
	Max
};


class ONLINESHOOTER_API IILogable
{
	GENERATED_BODY()

public:
	void Log(ELogType Type);
protected:
	virtual void LogStatement(ELogType Type, ELogStatementType StatementType)=0;
	virtual void LogStateInternal()=0;
	virtual void LogDataInternal()=0;
};
