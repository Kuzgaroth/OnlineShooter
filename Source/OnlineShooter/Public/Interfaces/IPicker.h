﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "PickUps/PickUpData/PickUpData.h"
#include "IPicker.generated.h"

class IIPicker;

USTRUCT()
struct FPickerDataStruct
{
	GENERATED_BODY()

	TSubclassOf<UPickUpData> PickUpDataClass;
	IIPicker* PickerObj;

	FPickerDataStruct(TSubclassOf<UPickUpData> DataClass, IIPicker* Obj)
	{
		PickUpDataClass = DataClass;
		PickerObj = Obj;
	}
	FPickerDataStruct()
	{
		PickUpDataClass = nullptr;
		PickerObj = nullptr;
	}
	inline bool operator==(const FPickerDataStruct& Struct2) const
	{
		return PickUpDataClass.Get()==Struct2.PickUpDataClass.Get() && PickerObj==Struct2.PickerObj;
	}
	
	
};

UINTERFACE(Blueprintable, BlueprintType)
class UIPicker : public UInterface
{
	GENERATED_BODY()
};

class ONLINESHOOTER_API IIPicker
{
	GENERATED_BODY()

public:
	virtual bool TryPickUp(UPickUpData* Data)=0;
	virtual void GetPickableDataClasses(TArray<FPickerDataStruct>& PickableClasses)=0;
protected:
	virtual bool CanPickUp()=0;
};
