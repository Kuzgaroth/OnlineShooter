﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PickUpBase.h"
#include "HealthPickUp.generated.h"

UCLASS()
class ONLINESHOOTER_API AHealthPickUp : public APickUpBase
{
	GENERATED_BODY()

public:
	AHealthPickUp();

protected:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Data")
	FHealthPickUpStruct HealthStructData;

	virtual void SetupData() override;
public:
	
};
