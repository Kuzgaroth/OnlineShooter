﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interfaces/ILogable.h"
#include "PickUpCoreTypes.h"
#include "PickUpBase.generated.h"

class UPickUpData;
class USphereComponent;

DECLARE_LOG_CATEGORY_EXTERN(LogPickUpBase, All, All)

UCLASS()
class ONLINESHOOTER_API APickUpBase : public AActor, public IILogable
{
	GENERATED_BODY()

public:
	APickUpBase();

protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Trigger")
	USphereComponent* TriggerComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Settings")
	bool bPickable=true;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Data")
	TSubclassOf<UPickUpData> PickUpDataClass;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Data")
	FBasePickUpStruct BaseStructData;
	
	virtual void BeginPlay() override;
	virtual void PostInitializeComponents() override;
	virtual void LogDataInternal() override;
	virtual void LogStateInternal() override;
	virtual void LogStatement(ELogType Type, ELogStatementType StatementType) override;
	virtual UPickUpData* GetPickUpData() const;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
	virtual void SetupData();

	UPROPERTY(Transient)
	UPickUpData* PickUpData;
public:
	virtual bool CanBePickedUp();
	virtual void OnPickedUp();
private:
	UFUNCTION()
	void OnTriggerOverlapped(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
		UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	bool bPickedUp=false;
	
	
};
