﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PickUpCoreTypes.generated.h"

USTRUCT(BlueprintType)
struct FBasePickUpStruct
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Data")
	bool bSingleUse=true;
};

USTRUCT(BlueprintType)
struct FHealthPickUpStruct
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Data")
	float MaxHealthPoint=100.f;
};