﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PickUpData.h"
#include "HealthPickUpData.generated.h"

UCLASS()
class ONLINESHOOTER_API UHealthPickUpData : public UPickUpData
{
	GENERATED_BODY()
public:
	UHealthPickUpData();

	void SetMaxHealthPoints(float MaxPoints);
protected:
	
public:
	float& GetHealthPoints() {return HealthPoints;}
	virtual void GetDataLog(TArray<FString>& DataArray) override;
	virtual void RefreshData() override;
	virtual void ClearData() override;
private:
	float HealthPoints=0.f;
	float MaxHealthPoints=0.f;
};
