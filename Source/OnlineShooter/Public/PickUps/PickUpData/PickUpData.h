﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Object.h"
#include "PickUpData.generated.h"

UCLASS(Blueprintable, BlueprintType)
class ONLINESHOOTER_API UPickUpData : public UObject
{
	GENERATED_BODY()

public:
	FORCEINLINE bool IsSingleUse() const {return bSingleUse;}
	FORCEINLINE virtual void ClearData(){bCleared=true;}
	FORCEINLINE bool IsCleared() const {return bCleared;}
	
	virtual void RefreshData(){bCleared=false;};
	FORCEINLINE void SetSingleUse(bool bNewSingleUse){bSingleUse=bNewSingleUse;}
	virtual void GetDataLog(TArray<FString>& DataArray);
private:
	bool bCleared=false;
	bool bSingleUse=false;
};
